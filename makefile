# Auteur : SOULEZ-DAMAZIE
# Date : 23 mai 2022
# Résumé : makefile pour la compilation des fichiers

img: fonction.o main.o
	@echo "compilation de l'executable"
	gcc fonction.o main.o -o img -Wall

fonction.o: fonction.c fonction.h
	@echo "compilation des fonctions"
	gcc -c fonction.c -o fonction.o -Wall

main.o : main.c fonction.h
	@echo :"compilation du main"
	gcc -c main.c -o main.o -Wall

clean:
	@echo "J'ai tout effacé !"
	rm -f *.o
