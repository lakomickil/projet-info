AUTEURS :
  Groupe 2, équipe Rojnivok d'IKEA
  - Soulez-Damazie Soraya
  - Lakomicki Laura

INTRUCTIONS DU LOGICIEL :

L'exécutable est nommé "img" : commencez donc par lancer le programme par "./img"
Puis ajoutez les commandes suivantes en fonction de vos besoins :

-h : Aide, renvoie la liste des commandes disponibles
-i 'fichier' : Définit le fichier d'entrée, OBLIGATOIRE (sauf si -h utilisé)
-o 'fichier' : Définit le fichier de sortie
-x 'largeur' 'hauteur' 'épaisseur' : Réalise une croix aux dimensions spécifiés
-g : Convertit l'image en niveau de gris
-b 'seuil' : Binarise une image (spécifier le seuil)
-m : Réalise le miroir d'une image
-p : Réalise une rotation de 90° dans le sens horaire
-n : Réalise le négatif d'une image
-r : Réalise un recadrage dynamique
-c : Réalise un renforcement de contraste
-f : Réalise un flou
-l : Réalise une détection des contours / lignes
-e : Réalise une érosion
-d : Réalise une dilatation
