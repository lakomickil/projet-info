//   FONCTION  . C
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fonction.h"


 img* chargement(FILE* fichierImage){
   img* image;
   image = malloc(sizeof(img)*1);
   int i=0;
   int j=0;

   image->format = malloc( sizeof(char)*10 );
   image->imageTab = malloc( sizeof(pixel*)*MAX );
   for (int i=0 ; i<MAX ; i++){
     image->imageTab[i] = malloc( sizeof(pixel)*MAX);
   }


   if ( fichierImage==NULL ){
     printf(" ERREUR OUVERTURE FICHIER \n");
   }else{
     // On récupère le début d'un fichier .ppm, aka : le format, la taille et la valeur max
     fscanf(fichierImage, "%s %d %d %d", image->format, &image->largeur, &image->hauteur, &image->valMax);

     for(i=0 ; i<image->hauteur ; i++){  //i correspond aux lignes

       for( j=0 ; j<image->largeur; j++){  //j correspond aux colonnes
         fscanf(fichierImage, "%d", &image->imageTab[i][j].rouge); //valeur rouge du pixel n° "laLigne"
         fscanf(fichierImage, "%d", &image->imageTab[i][j].vert);
         fscanf(fichierImage, "%d", &image->imageTab[i][j].bleu);
       };

     };
     fclose(fichierImage);
     /*  POUR TEST :
     printf(" test, début du fichier: %s\n %d\n %d\n %d\n", image.format, image.largeur, image.longueur, image.valMax);
     printf(" valeur 1er pixel : %d %d %d\n", image.imageTab[0][0].rouge, image.imageTab[0][0].vert, image.imageTab[0][0].bleu);
     printf(" valeur 3e pixel : %d %d %d\n", image.imageTab[0][2].rouge, image.imageTab[0][2].vert, image.imageTab[0][2].bleu);
     printf("i: %d, j: %d\n", i, j);
     */
     }
   return image;
 }


int sauvegarde(img* image, char* nameNewFile){
  int res = 0;
  FILE* imageTrans = NULL;
  imageTrans = fopen(nameNewFile,"w");

  if (imageTrans == NULL){
    printf("Erreur ouverture fichier \n");
  }else{
    fprintf(imageTrans, "%s\n", image->format);
    fprintf(imageTrans, "%d %d\n", image->largeur, image->hauteur);
    fprintf(imageTrans, "%d\n", image->valMax);
    for(int i=0 ; i<image->hauteur ; i++){
      for(int j=0 ; j<image->largeur ; j++){
        // deux espaces séparent chaque pixel
        fprintf(imageTrans, "%d %d %d  ", image->imageTab[i][j].rouge,image->imageTab[i][j].vert, image->imageTab[i][j].bleu );
      }
      fprintf(imageTrans,"\n");
    }
    res = 1;
  };

  fclose(imageTrans);
  return res;
 }


void creation(FILE* fichierCroix, int x, int y, int ep){
  // initialisation de notre img croix
  img* croix;
  croix = malloc(sizeof(img)*1);

  croix->format = malloc(sizeof(char)*10);
  croix->format = "P3";
  croix->hauteur = y;
  croix->largeur = x;
  croix->valMax = 255;
  croix->imageTab = malloc( sizeof(pixel*)*croix->hauteur);
  for (int i=0; i<MAX ; i++){
    croix->imageTab[i] = malloc(sizeof(pixel)*croix->largeur);
  }


  // On remplit d'abord l'image de pixels blanc
  for(int i=0 ; i<croix->hauteur ; i++){
    for(int j=0 ; j<croix->largeur ; j++){
      croix->imageTab[i][j].rouge = 255;
      croix->imageTab[i][j].vert = 255;
      croix->imageTab[i][j].bleu = 255;
    }
  }

  // note : si le côté est pair, alors l'épaisseur doit l'être aussi ; et inversement.
  // sinon, la croix est décalée ou juste inexistante.

  //barre verticale
  if( x%2 == 0){
    for(int i=(x/2)-(ep/2) ; i<(x/2)+(ep/2) ; i++){
      for(int j=0; j<y; j++){
        croix->imageTab[i][j].rouge = 0;
        croix->imageTab[i][j].vert = 0;
        croix->imageTab[i][j].bleu = 0;
      }
    }
  }else{
    for(int i=(x/2)-(ep/2) ; i<=(x/2)+(ep/2) ; i++){
      for(int j=0; j<y; j++){
        croix->imageTab[i][j].rouge = 0;
        croix->imageTab[i][j].vert = 0;
        croix->imageTab[i][j].bleu = 0;
      }
    }
  }

  //barre horizontale
  if( y%2 == 0){
    for(int i=(y/2)-(ep/2) ; i<(y/2)+(ep/2) ; i++){
      for(int j=0; j<x; j++){
        croix->imageTab[j][i].rouge = 0;
        croix->imageTab[j][i].vert = 0;
        croix->imageTab[j][i].bleu = 0;
      }
    }
  }else{
    for(int i=(y/2)-(ep/2) ; i<=(y/2)+(ep/2) ; i++){
      for(int j=0; j<x; j++){
        croix->imageTab[j][i].rouge = 0;
        croix->imageTab[j][i].vert = 0;
        croix->imageTab[j][i].bleu = 0;
      }
    }
  }


  // Sauvegarde dans le fichier Croix :
  if (fichierCroix == NULL){
    printf("Erreur ouverture fichier \n");
  }else{
    fprintf(fichierCroix, "%s\n", croix->format);
    fprintf(fichierCroix, "%d %d\n", croix->largeur, croix->hauteur);
    fprintf(fichierCroix, "%d\n", croix->valMax);
    for(int i=0 ; i<croix->hauteur ; i++){
      for(int j=0 ; j<croix->largeur ; j++){
        // deux espaces séparent chaque pixel
        fprintf(fichierCroix, "%d %d %d  ", croix->imageTab[i][j].rouge,croix->imageTab[i][j].vert, croix->imageTab[i][j].bleu );
      }
      fprintf(fichierCroix,"\n");
    }
  };
  fclose(fichierCroix);

}


void nb(img* image){
  int luminanceGris = 0;
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      luminanceGris = 0.2126*image->imageTab[i][j].rouge + 0.7152*image->imageTab[i][j].vert + 0.0722*image->imageTab[i][j].bleu ;
      image->imageTab[i][j].rouge = luminanceGris;
      image->imageTab[i][j].vert = luminanceGris;
      image->imageTab[i][j].bleu = luminanceGris;
    }
  }
}


void binarisation(img* image, int seuil){
  int moyenne = 0;
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      moyenne = ( image->imageTab[i][j].rouge+image->imageTab[i][j].vert+image->imageTab[i][j].bleu ) / 3;
      if (moyenne > seuil){
        // Le pixel est alors blanc
        image->imageTab[i][j].rouge = 255;
        image->imageTab[i][j].vert = 225;
        image->imageTab[i][j].bleu = 255;
      }else{
        // Le pixel est alors noir
        image->imageTab[i][j].rouge = 0;
        image->imageTab[i][j].vert = 0;
        image->imageTab[i][j].bleu = 0;
      }
    }
  }
}


void miroir(img* image){
  int tmp = 0;
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j< (image->largeur/2) ; j++){
      // On arrête les échanges à la moitié de l'image
      // Echanges :
      tmp = image->imageTab[i][j].rouge;
      image->imageTab[i][j].rouge = image->imageTab[i][image->largeur-j].rouge;
      image->imageTab[i][image->largeur-j].rouge = tmp;

      tmp = image->imageTab[i][j].vert;
      image->imageTab[i][j].vert = image->imageTab[i][image->largeur-j].vert;
      image->imageTab[i][image->largeur-j].vert = tmp;

      tmp = image->imageTab[i][j].bleu;
      image->imageTab[i][j].bleu = image->imageTab[i][image->largeur-j].bleu;
      image->imageTab[i][image->largeur-j].bleu = tmp;
    }
  }
}


void rotation(img* image){

  // initialisation de la copie qui contendra l'image tournée :
  img* rota;
  rota = malloc(sizeof(img)*1);

  rota->format = malloc( sizeof(char)*10 );
  rota->format = image->format;
  rota->hauteur = image->largeur;
  rota->largeur = image->hauteur;
  rota->valMax = image->valMax;

  rota->imageTab = malloc( sizeof(pixel*)*MAX );
  for (int i=0 ; i<MAX ; i++){
    rota->imageTab[i] = malloc( sizeof(pixel)*MAX );
  }

  // Les lignes de bas en haut de "image" deviennent les colonnes de droite à gauche de "rota"
  for(int i=0 ; i<rota->hauteur ; i++){
    for(int j=0 ; j<rota->largeur ; j++){
      rota->imageTab[i][j].rouge =  image->imageTab[image->hauteur-j][i].rouge;
      rota->imageTab[i][j].vert =  image->imageTab[image->hauteur-j][i].vert;
      rota->imageTab[i][j].bleu =  image->imageTab[image->hauteur-j][i].bleu;

    }
  }

  // Comme 'rotation' est une procédure, 'image' prend les valeurs de 'rota'
  image->largeur = rota->largeur;
  image->hauteur = rota->hauteur;
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      image->imageTab[i][j].rouge = rota->imageTab[i][j].rouge;
      image->imageTab[i][j].vert = rota->imageTab[i][j].vert;
      image->imageTab[i][j].bleu = rota->imageTab[i][j].bleu;
    }
  }


}


void negatif(img* image){
  int luminanceMax = 0;
  int lumPixel = 0;
  //Luminance max
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      lumPixel = 0.2126*image->imageTab[i][j].rouge + 0.7152*image->imageTab[i][j].vert + 0.0722*image->imageTab[i][j].bleu ;
      if(lumPixel > luminanceMax){
        luminanceMax = lumPixel ;
      }
    }
  }
  // Négatif :
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      //on ajoute +1 pour éviter les -1
      image->imageTab[i][j].rouge = luminanceMax+1 - image->imageTab[i][j].rouge;
      image->imageTab[i][j].vert = luminanceMax+1 - image->imageTab[i][j].vert;
      image->imageTab[i][j].bleu = luminanceMax+1 - image->imageTab[i][j].bleu;
    }
  }
}


void historigramme(img* image){
  negatif(image);
  int* tabLuminance ;
  tabLuminance = malloc( sizeof(int)*255);
  // On initialise toutes les valeurs du tableau à 0
  for(int k=0 ; k<256 ; k++){
    tabLuminance[k] = 0;
  }
  // On ajoute +1 dans la case de luminance correspondante
  // La valeur du rouge, du vert et du bleu est la même comme l'image est en noir et blanc
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      tabLuminance[ image->imageTab[i][j].rouge ] ++;
    }
  }
  // Affichage du tableau :
  printf("  Historigramme :\n");
  for(int k=0 ; k<256 ; k++){
    printf("Luminance de %d : %d pixels\n", k, tabLuminance[k]);
  }

  free(tabLuminance);
}


void recadrageDynamique(img* image){
  // Calculons la lumiMAX et lumiMIN :
  int lumiMAX = 0;
  int lumiMIN = 255;
  int lumiTmp = 0;
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      lumiTmp = 0.2126*image->imageTab[i][j].rouge + 0.7152*image->imageTab[i][j].vert + 0.0722*image->imageTab[i][j].bleu;
      if (lumiTmp > lumiMAX){
        lumiMAX = lumiTmp;
      }
      if (lumiTmp < lumiMIN){
        lumiMIN = lumiTmp;
      }
    }
  }
  // Calcul du delta et de la nouvelle luminance de chaque pixel
  int delta;
  delta = 255 / (lumiMAX - lumiMIN);
  for(int i=0 ; i<image->hauteur ; i++){
    for(int j=0 ; j<image->largeur ; j++){
      lumiTmp = 0.2126*image->imageTab[i][j].rouge + 0.7152*image->imageTab[i][j].vert + 0.0722*image->imageTab[i][j].bleu;
      lumiTmp = (lumiTmp - lumiMIN) * delta;
      image->imageTab[i][j].rouge = lumiTmp;
      image->imageTab[i][j].vert = lumiTmp;
      image->imageTab[i][j].bleu = lumiTmp;
    }
  }


}


void convolution(img* image, double** matrice){

  // initialisation de la copie qui contendra l'image filtrée :
  img* avecFiltre;
  avecFiltre = malloc(sizeof(img)*1);

  avecFiltre->format = malloc( sizeof(char)*10 );
  avecFiltre->format = image->format;
  avecFiltre->hauteur = image->hauteur;
  avecFiltre->largeur = image->largeur;
  avecFiltre->valMax = image->valMax;

  avecFiltre->imageTab = malloc( sizeof(pixel*)*MAX );
  for (int i=0 ; i<MAX ; i++){
    avecFiltre->imageTab[i] = malloc( sizeof(pixel)*MAX);
  }

  // On ajoute une ligne à chaque bord de l'image pour éviter les prblm de segmentations
  // Pas besoin de reallouer étant donnée que l'allocation (MAX) est déjà supérieure à la taille de l'image normalement
  image->hauteur = image->hauteur + 2;
  image->largeur = image->largeur + 2;
  // Décalage des valeurs du tableau (comme on ajoute une ligne tout en haut et une tout à gauche) :
  for(int i=image->hauteur-1 ; i>=0 ; i--){
    for(int j=image->largeur-1 ; j>=0 ; j--){
      image->imageTab[i+1][j+1].rouge = image->imageTab[i][j].rouge;
      image->imageTab[i+1][j+1].vert = image->imageTab[i][j].vert;
      image->imageTab[i+1][j+1].bleu = image->imageTab[i][j].bleu;
    }
  };
  // Valeurs des bords :
  for(int j=1 ; j<image->largeur-1 ; j++){
    image->imageTab[0][j].rouge = 0;
    image->imageTab[0][j].vert = 0;
    image->imageTab[0][j].bleu = 0;
    image->imageTab[image->hauteur-1][j].rouge = 0;
    image->imageTab[image->hauteur-1][j].vert = 0;
    image->imageTab[image->hauteur-1][j].bleu = 0;
  }
  for(int i=0 ; i<image->hauteur ; i++){
    image->imageTab[i][0].rouge = 0;
    image->imageTab[i][0].vert = 0;
    image->imageTab[i][0].bleu = 0;
    image->imageTab[i][image->largeur-1].rouge = 0;
    image->imageTab[i][image->largeur-1].vert = 0;
    image->imageTab[i][image->largeur-1].bleu = 0;
  }

  // Produit de convolution :
  pixel tmp;
  for(int i=1 ; i<avecFiltre->hauteur ; i++){
    for(int j=1 ; j<avecFiltre->largeur ; j++){

      tmp.rouge = 0;
      tmp.vert = 0;
      tmp.bleu = 0;
      for(int k=0 ; k<3 ; k++){
        for(int l=0 ; l<3 ; l++){
          tmp.rouge = tmp.rouge + ( image->imageTab[i-1+k][j-1+l].rouge * matrice[k][l] );
          tmp.vert = tmp.vert + ( image->imageTab[i-1+k][j-1+l].vert * matrice[k][l] );
          tmp.bleu = tmp.bleu + ( image->imageTab[i-1+k][j-1+l].bleu * matrice[k][l] );
        }
      }
      // Il faut impativement que les pixels aient des valeurs entre 0 et 255 compris
      if(tmp.rouge < 0){
        tmp.rouge = 0;
      }
      if(tmp.rouge > 255 ){
        tmp.rouge = 255;
      }
      if(tmp.vert < 0){
        tmp.vert = 0;
      }
      if(tmp.vert > 255 ){
        tmp.vert = 255;
      }
      if(tmp.bleu < 0){
        tmp.bleu = 0;
      }
      if(tmp.bleu > 255){
        tmp.bleu = 255;
      }

      avecFiltre->imageTab[i][j].rouge = tmp.rouge;
      avecFiltre->imageTab[i][j].vert = tmp.vert;
      avecFiltre->imageTab[i][j].bleu = tmp.bleu;

    }
  }

  // image prend les valeurs de avecFiltre maintenant (comme c'est une procédure)
  for(int i=0 ; i<avecFiltre->hauteur ; i++){
    for(int j=0 ; j<avecFiltre->largeur ; j++){
      image->imageTab[i][j].rouge = avecFiltre->imageTab[i][j].rouge;
      image->imageTab[i][j].vert = avecFiltre->imageTab[i][j].vert;
      image->imageTab[i][j].bleu = avecFiltre->imageTab[i][j].bleu;
    }
  }

  // On supprime les 2 lignes tout en bas et tout à droite pour remettre à la bonne taille
  // Pour cela, changer la taille supposée de l'image marche
  image->hauteur = image->hauteur - 2;
  image->largeur = image->largeur - 2;
}


void renfContraste(img* image){
  // Matrice de convolution 3x3 pour le renforcement de contraste :
  double** matriceR;
  matriceR = malloc( sizeof(double*)*3 );
  for (int i=0 ; i<3 ; i++){
    matriceR[i] = malloc( sizeof(double)*3 );
  }
  matriceR[0][0] = 0;
  matriceR[0][1] = -1;
  matriceR[0][2] = 0;
  matriceR[1][0] = -1;
  matriceR[1][1] = 5;
  matriceR[1][2] = -1;
  matriceR[2][0] = 0;
  matriceR[2][1] = -1;
  matriceR[2][2] = 0;

  convolution(image, matriceR);
}


void floutage(img* image){
  // Matrice de convolution 3x3 pour le floutage :
  double** matriceF;
  matriceF = malloc( sizeof(double*)*3 );
  for (int i=0 ; i<3 ; i++){
    matriceF[i] = malloc( sizeof(double)*3 );
  }
  matriceF[0][0] = 0.0625;
  matriceF[0][1] = 0.0125;
  matriceF[0][2] = 0.0625;
  matriceF[1][0] = 0.125;
  matriceF[1][1] = 0.25;
  matriceF[1][2] = 0.125;
  matriceF[2][0] = 0.0625;
  matriceF[2][1] = 0.125;
  matriceF[2][2] = 0.0625;

  convolution(image, matriceF);
}


void contour(img* image){
  // Matrice de convolution 3x3 pour le contour :
  double** matriceC;
  matriceC = malloc( sizeof(double*)*3 );
  for (int i=0 ; i<3 ; i++){
    matriceC[i] = malloc( sizeof(double)*3 );
  }
  for (int i=0 ; i<3 ; i++){
    for (int j=0 ; j<3 ; j++){
      matriceC[i][j] = -1;
    }
  }
  matriceC[1][1] = 8;

  convolution(image, matriceC);
}


void erosion(img* image, img* croix){
  // on travaille sur des images en noir et blanc
  nb(image);

  // initialisation de la copie qui contendra l'image filtrée :
  img* ero;
  ero = malloc(sizeof(img)*1);

  ero->format = malloc( sizeof(char)*10 );
  ero->format = image->format;
  ero->hauteur = image->hauteur;
  ero->largeur = image->largeur;
  ero->valMax = image->valMax;
  ero->imageTab = malloc( sizeof(pixel*)*MAX );
  for (int i=0 ; i<MAX ; i++){
    ero->imageTab[i] = malloc( sizeof(pixel)*MAX);
  }

  // On ajoute une ligne à chaque bord de l'image pour éviter les prblm de segmentations
  // Pas besoin de reallouer étant donnée que l'allocation (MAX) est déjà supérieure à la taille de l'image normalement
  image->hauteur = image->hauteur + 2;
  image->largeur = image->largeur + 2;
  // Décalage des valeurs du tableau (comme on ajoute une ligne tout en haut et une tout à gauche) :
  for(int i=image->hauteur-1 ; i>=0 ; i--){
    for(int j=image->largeur-1 ; j>=0 ; j--){
      image->imageTab[i+1][j+1].rouge = image->imageTab[i][j].rouge;
      image->imageTab[i+1][j+1].vert = image->imageTab[i][j].vert;
      image->imageTab[i+1][j+1].bleu = image->imageTab[i][j].bleu;
    }
  };
  // Valeurs des bords à 255 pour que l'érosion ne les prenne pas en compte :
  for(int j=1 ; j<image->largeur-1 ; j++){
    image->imageTab[0][j].rouge = 255;
    image->imageTab[0][j].vert = 255;
    image->imageTab[0][j].bleu = 255;
    image->imageTab[image->hauteur-1][j].rouge = 255;
    image->imageTab[image->hauteur-1][j].vert = 255;
    image->imageTab[image->hauteur-1][j].bleu = 255;
  }
  for(int i=0 ; i<image->hauteur ; i++){
    image->imageTab[i][0].rouge = 255;
    image->imageTab[i][0].vert = 255;
    image->imageTab[i][0].bleu = 255;
    image->imageTab[i][image->largeur-1].rouge = 255;
    image->imageTab[i][image->largeur-1].vert = 255;
    image->imageTab[i][image->largeur-1].bleu = 255;
  }

  // érosion :
  int min;
  for(int i=1 ; i<image->hauteur ; i++){
    for(int j=1 ; j<image->largeur ; j++){
      min = 255;

      // on compare les valeurs autour avec la croix fournie pour chercher la luminance minimale parmi les pixel où il y a la croix
      // l'image étant en nb, on peut faire les comparaisons que sur les pixels rouges
      for(int k=0 ; k<croix->hauteur ; k++){
        for(int l=0 ; l<croix->largeur ; l++){
          if( (croix->imageTab[k][l].rouge == 0) && (image->imageTab[i-1+k][j-1+k].rouge < min) ){
            min = image->imageTab[i-1+k][j-1+k].rouge;
          }
        }
      }

      // On met alors la valeur min dans le pixel érodé, sans oublié que l'index de ero est décalé :
      ero->imageTab[i-1][j-1].rouge = min;
      ero->imageTab[i-1][j-1].vert = min;
      ero->imageTab[i-1][j-1].bleu = min;
    }
  }

  // image prend les valeurs de ero maintenant (comme c'est une procédure)
  for(int i=0 ; i<ero->hauteur ; i++){
    for(int j=0 ; j<ero->largeur ; j++){
      image->imageTab[i][j].rouge = ero->imageTab[i][j].rouge;
      image->imageTab[i][j].vert = ero->imageTab[i][j].vert;
      image->imageTab[i][j].bleu = ero->imageTab[i][j].bleu;
    }
  }

  // On supprime les 2 lignes tout en bas et tout à droite pour remettre à la bonne taille
  // Pour cela, changer la taille supposée de l'image marche
  image->hauteur = image->hauteur - 2;
  image->largeur = image->largeur - 2;

}


void dilatation(img* image, img* croix){
  // on travaille sur des images en noir et blanc
  nb(image);

  // initialisation de la copie qui contendra l'image filtrée :
  img* dila;
  dila = malloc(sizeof(img)*1);
  dila->format = malloc( sizeof(char)*10 );
  dila->format = image->format;
  dila->hauteur = image->hauteur;
  dila->largeur = image->largeur;
  dila->valMax = image->valMax;
  dila->imageTab = malloc( sizeof(pixel*)*MAX );
  for (int i=0 ; i<MAX ; i++){
    dila->imageTab[i] = malloc( sizeof(pixel)*MAX);
  }

  // On ajoute une ligne à chaque bord de l'image pour éviter les prblm de segmentations
  // Pas besoin de reallouer étant donnée que l'allocation (MAX) est déjà supérieure à la taille de l'image normalement
  image->hauteur = image->hauteur + 2;
  image->largeur = image->largeur + 2;
  // Décalage des valeurs du tableau (comme on ajoute une ligne tout en haut et une tout à gauche) :
  for(int i=image->hauteur-1 ; i>=0 ; i--){
    for(int j=image->largeur-1 ; j>=0 ; j--){
      image->imageTab[i+1][j+1].rouge = image->imageTab[i][j].rouge;
      image->imageTab[i+1][j+1].vert = image->imageTab[i][j].vert;
      image->imageTab[i+1][j+1].bleu = image->imageTab[i][j].bleu;
    }
  };
  // Valeurs des bords à 0 pour que la dilatation ne les prenne pas en compte :
  for(int j=1 ; j<image->largeur-1 ; j++){
    image->imageTab[0][j].rouge = 0;
    image->imageTab[0][j].vert = 0;
    image->imageTab[0][j].bleu = 0;
    image->imageTab[image->hauteur-1][j].rouge = 0;
    image->imageTab[image->hauteur-1][j].vert = 0;
    image->imageTab[image->hauteur-1][j].bleu = 0;
  }
  for(int i=0 ; i<image->hauteur ; i++){
    image->imageTab[i][0].rouge = 0;
    image->imageTab[i][0].vert = 0;
    image->imageTab[i][0].bleu = 0;
    image->imageTab[i][image->largeur-1].rouge = 0;
    image->imageTab[i][image->largeur-1].vert = 0;
    image->imageTab[i][image->largeur-1].bleu = 0;
  }

  // dilatation :
  int max;
  for(int i=1 ; i<image->hauteur ; i++){
    for(int j=1 ; j<image->largeur ; j++){
      max = 0;

      // on compare les valeurs autour avec la croix fournie pour chercher la luminance maximale parmi les pixel où il y a la croix
      // l'image étant en nb, on peut faire les comparaisons que sur les pixels rouges
      for(int k=0 ; k<croix->hauteur ; k++){
        for(int l=0 ; l<croix->largeur ; l++){
          if( (croix->imageTab[k][l].rouge == 0) && (image->imageTab[i-1+k][j-1+k].rouge > max) ){
            max = image->imageTab[i-1+k][j-1+k].rouge;
          }
        }
      }

      // On met alors la valeur min dans le pixel érodé, sans oublié que l'index de ero est décalé :
      dila->imageTab[i-1][j-1].rouge = max;
      dila->imageTab[i-1][j-1].vert = max;
      dila->imageTab[i-1][j-1].bleu = max;
    }
  }

  // image prend les valeurs de dila maintenant (comme c'est une procédure)
  for(int i=0 ; i<dila->hauteur ; i++){
    for(int j=0 ; j<dila->largeur ; j++){
      image->imageTab[i][j].rouge = dila->imageTab[i][j].rouge;
      image->imageTab[i][j].vert = dila->imageTab[i][j].vert;
      image->imageTab[i][j].bleu = dila->imageTab[i][j].bleu;
    }
  }

  // On supprime les 2 lignes tout en bas et tout à droite pour remettre à la bonne taille
  // Pour cela, changer la taille supposée de l'image marche
  image->hauteur = image->hauteur - 2;
  image->largeur = image->largeur - 2;

}
