//   MAIN   . C
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "fonction.h"


int main(int argc, char* argv[]){

  int result = 0; // result =1 si le programme a fonctionné, 0 sinon ou si on a juste demandé l'aide
  int error = 1; // pour savoir si on a eu une erreur quand return=0 ou si on a juste ouvert l'aide


  // Affiche l'aide
  if ( ( (argc>1)&&(!strcmp(argv[1],"-h")) ) || (argc == 1) ){
    printf("-----------------------------------------\n");
    printf("   AIDE PROGRAMME :\n\n");
    printf(" -h : Aide, renvoie la liste des commandes disponibles\n");
    printf(" -i 'fichier' : Définit le fichier d'entrée, OBLIGATOIRE (sauf si -h utilisé)\n");
    printf(" -o 'fichier' : Définit le fichier de sortie\n");
    printf(" -x 'largeur' 'hauteur' 'épaisseur' : Réalise une croix aux dimensions spécifiés\n");
    printf(" -g : Convertit l'image en niveau de gris\n");
    printf(" -b 'seuil' : Binarise une image (spécifier le seuil)\n");
    printf(" -m : Réalise le miroir d'une image\n");
    printf(" -p : Réalise une rotation de 90° dans le sens horaire\n");
    printf(" -n : Réalise le négatif d'une image\n");
    printf(" -r : Réalise un recadrage dynamique\n");
    printf(" -c : Réalise un renforcement de contraste\n");
    printf(" -f : Réalise un flou\n");
    printf(" -l : Réalise une détection des contours / lignes\n");
    printf(" -e : Réalise une érosion\n");
    printf(" -d : Réalise une dilatation\n\n");
    printf("-----------------------------------------\n");
    error = 0;
  }


  // Chargement de l'image :
  img* image;
  for (int i=1 ; i<argc ; i++){
    // on verifie que la chaine qui suit contient bien un mot avec ".ppm" :
    if(  (!strcmp(argv[i],"-i")) && ( strstr(argv[i+1], ".ppm") )  ){
      FILE* imageBase = NULL;
      imageBase = fopen(argv[i+1],"r");

      image = chargement( imageBase );
      i = i+2;
      result = 1;
    }
  }




  int i=0;
  if(result ==1 /* ie: on a eu '-i'*/ ){
    int continuer = argc-1; // = nb d'arguments autre que le ./img

    // on commence à 1 étant donné que argv[0]= "./img"
    while(continuer > 0){
      // pour sauter l'argument du chargement d'image :
      if(!strcmp(argv[i],"-i")){
        i++;
        continuer --;
      }
      // Croix :
      if(!strcmp(argv[i],"-x")){
        FILE* fichierCroix = NULL;
        fichierCroix = fopen("croix.ppm","w");
        int x, y, ep;
        y = atoi(argv[i+1]);
        x = atoi(argv[i+2]);
        ep = atoi(argv[i+3]);
        creation(fichierCroix, x, y, ep);
        i = i+3;
        continuer = continuer-3;
      }
      // Noir et blanc :
      if(!strcmp(argv[i],"-g")){
        nb(image);
        continuer--;
      }
      // Binarisation :
      if( (!strcmp(argv[i],"-b")) && (continuer>1) ){
        int seuil;
        seuil = atoi(argv[i+1]);
        binarisation(image, seuil);
        i++;
        continuer--;
      }
      // Miroir :
      if(!strcmp(argv[i],"-m")){
        miroir(image);
      }
      // Rotation :
      if(!strcmp(argv[i],"-p")){
        rotation(image);
      }
      // Négatif :
      if(!strcmp(argv[i],"-n")){
        negatif(image);
      }
      // Recadrage dynamique :
      if(!strcmp(argv[i],"-r")){
        recadrageDynamique(image);
      }
      // Renforcement contraste:
      if(!strcmp(argv[i],"-c")){
        renfContraste(image);
      }
      // Flou :
      if(!strcmp(argv[i],"-f")){
        floutage(image);
      }
      // Détection des contours / lignes:
      if(!strcmp(argv[i],"-l")){
        contour(image);
      }
      // Erosion :
      if(!strcmp(argv[i],"-e")){
        // création de la croix de 5 pour l'érosion
        int x = 3;
        int y = 3;
        int ep = 1;
        FILE* fichierCroix = NULL;
        fichierCroix = fopen("croix.ppm","w");
        creation(fichierCroix, x, y, ep);

        img* croix;
        fichierCroix = fopen("croix.ppm","r");
        croix = chargement(fichierCroix);
        erosion(image, croix);
      }
      // Dilatation :
      if(!strcmp(argv[i],"-d")){
        // création de la croix de 5 pour la dilatation
        int x = 3;
        int y = 3;
        int ep = 1;
        FILE* fichierCroix = NULL;
        fichierCroix = fopen("croix.ppm","w");
        creation(fichierCroix, x, y, ep);

        img* croix;
        fichierCroix = fopen("croix.ppm","r");
        croix = chargement(fichierCroix);
        dilatation(image, croix);
      }

      i++;
      continuer--;
    }


    // Sauvegarde ( si elle est demandé, sinon on affcihe les pixels) :
    int aff = 1;
    for (i=1 ; i<argc ; i++){
      // on verifie que la chaine qui suit contient bien un mot avec ".ppm" :
      if(  (!strcmp(argv[i],"-o")) && ( strstr(argv[i+1], ".ppm") )  ){
        result = sauvegarde(image, argv[i+1]);
        aff = 0;
      }
    }
    if (aff){
      printf(" Affichage du contenu de la nouvelle image :\n");
      printf("%s\n", image->format);
      printf("%d %d\n", image->largeur, image->hauteur);
      printf("%d\n", image->valMax);
      for(int i=0 ; i<image->hauteur ; i++){
        for(int j=0 ; j<image->largeur ; j++){
          printf("%d %d %d  ",image->imageTab[i][j].rouge,image->imageTab[i][j].vert,image->imageTab[i][j].bleu);
        }
        printf("\n");
      }
    }

    // Free  :
    for(int i=0; i<image->hauteur ; i++){
      free(image->imageTab[i]);
    }
    free(image->imageTab);
    free(image);
  }



  if( (!result)&&(error)){
    printf(" ERREUR, LE PROGRAMME N'A PAS FONCTIONNÉ \n");
  }

  return result;
}


