//   FONCTION . H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifndef __fonction_H_
#define __fonction_H_
#define MAX 300

/* Auteur : Soraya
 * Date : 23 mai 2022
 * Résumé : Réalisation de la structure pixel et img*/
struct pixel{
  int rouge;
  int vert;
  int bleu;
};
typedef struct pixel pixel;

struct img{
  char* format;
  int largeur; //nb de colonnes de pixels
  int hauteur; //nb de lignes de pixels
  int valMax;
  pixel** imageTab;
};
typedef struct img img;



/* toutes mes signatures/prototypes de fonctions/méthodes */

/* Date : 24/25 mai 2022
  Auteur : Laura
  Resume : Récupère les données d'une image et les met dans un tableau */
 img* chargement(FILE* fichierImage);
 /* Auteur : Soraya
  * Date : 23 mai  2022
  * Résumé : Début de la fonction sauvegarde : récupère les données d'un tableau et les transforme en image .ppm
             Renvoie 1 si la sauvegarde a fonctionnée, 0 sinon
  ----
  * Auteur : Laura
  * Date : 26 mai 2022
  * Resume : fin fct sauvegarde*/
 int sauvegarde(img* image, char* nameNewFile);
 /* Auteur : Soraya
  * Date : 31 mai/3 juin 2022
  * Résumé : Créé une image représentant une croix au centre,
  * x est la largeur de l'image, y la hauteur(=longueur), epaisseur l'épaisseur de la croix
  */
  void creation(FILE* fichierCroix, int x, int y, int ep);
 /* Auteur : Laura
  * Date : 26 mai 2022
  * Résumé : Procédure changeant une image une noir et blanc */
  void nb(img* image);
 /* Auteur : Laura
  * Date : 27 mai 2022
  * Résumé : Procédure réalisant une binarisation */
  void binarisation(img* image, int seuil);
  /* Auteur : Laura
   * Date : 27 mai 2022
   * Résumé : Procédure réalisant le miroir vertical d'une image */
  void miroir(img* image);
  /* Auteur : Laura
   * Date : 31 mai 2022
   * Résumé : Procédure réalisant une rotation de 90° d'une image */
  void rotation(img* image);
  /* Auteur : Laura
   * Date : 27 mai 2022
   * Résumé : Procédure réalisant le négatif d'une image */
  void negatif(img* image);
  /* Auteur : Laura
   * Date : 28 mai 2022
   * Résumé : Procédure réalisant l'historigramme d'une image */
 void historigramme(img* image);
 /* Auteur :
  * Date :
  * Résumé : Procédure réalisant l'historigramme d'une image */
 void recadrageDynamique(img* image);
 /* Auteur : Laura
  * Date : 29/31 mai 2022
  * Résumé : Procédure réalisant le produit de convolution avec une matrice 3x3 donnée */
 void convolution(img* image, double** matrice);
 /* Auteur : Laura
  * Date : 30 mai 2022
  * Résumé : Procédure créant la matrice de convolution pour le contraste */
 void renfContraste(img* image);
 /* Auteur : Laura
  * Date : 30 mai 2022
  * Résumé : Procédure créant la matrice de convolution pour le floutage */
 void floutage(img* image);
 /* Auteur : Laura
  * Date : 30 mai 2022
  * Résumé : Procédure créant la matrice de convolution pour le contour */
 void contour(img* image);
 /* Auteur : Laura
  * Date : 4 juin 2022
  * Résumé : Procédure réalisant une érosion de img* image */
void erosion(img* image, img* croix);
/* Auteur : Laura
 * Date : 4 juin 2022
 * Résumé : Procédure réalisant une dilatation de img* image */
void dilatation(img* image, img* croix);









#endif
